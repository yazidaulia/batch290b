package com.miniproject.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/page/")
public class PageController {

	@GetMapping("initialpage")
	public ModelAndView initialpage() {
		ModelAndView view = new ModelAndView("page/initialpage");
		return view;
	}
	
	@GetMapping("landingpage")
	public ModelAndView landingpageadmin() {
		ModelAndView view = new ModelAndView("page/landingpage");
		return view;
	}
	
	@GetMapping("rizka")
	public ModelAndView rizka() {
		ModelAndView view = new ModelAndView("page/tes");
		return view;
	}
	
	// ASC	animals = animals.stream().sorted(Comparator.comparing(Animal::getName)).collect(Collectors.toList());
	// DESC animals = animals.stream().sorted(Comparator.comparing(Animal::getName).reversed()).collect(Collectors.toList());
}

package com.miniproject.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject.demo.model.Biodata;

public interface BiodataRepository extends JpaRepository<Biodata, Long> {
	List<Biodata> findByIsDelete(Boolean isDelete);
}
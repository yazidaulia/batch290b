package com.miniproject.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.AddressModel;
import com.miniproject.demo.model.Biodata;
import com.miniproject.demo.model.CategoryFasilitas;
import com.miniproject.demo.model.Customer;
import com.miniproject.demo.model.ResetPassword;
import com.miniproject.demo.model.User;
import com.miniproject.demo.repository.BiodataRepository;
import com.miniproject.demo.repository.CustomerRepository;
import com.miniproject.demo.repository.ResetPasswordRepository;
import com.miniproject.demo.repository.UserRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiTabProfileController {

	@Autowired
	BiodataRepository biodataRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	ResetPasswordRepository resetPasswordRepository;
	
	
	  @GetMapping("user") public ResponseEntity<List<User>> getAllUser(){
	 	try {
			List<User> user = this.userRepository.findByIsDelete(false);
			return new ResponseEntity<>(user, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	  /*
	@GetMapping("customer")
	public ResponseEntity<List<Customer>> getAllCustomer(){
		try {
			List<Customer> customer = this.customerRepository.findByIsDelete(false);
			return new ResponseEntity<>(customer, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping("biodata")
	public ResponseEntity<List<Biodata>> getAllBiodata(){
		try {
			List<Biodata> biodata = this.biodataRepository.findByIsDelete(false);
			return new ResponseEntity<>(biodata, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	*/
	
	//Controller Tab Profile
	@GetMapping("biodata/{id}")
	public ResponseEntity<List<Biodata>> getBiodataById(@PathVariable("id") Long id){
		try {
			Optional<Biodata> biodata = this.biodataRepository.findById(id);
			if(biodata.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(biodata, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<Biodata>>(HttpStatus.NO_CONTENT);
		}
	}
	
	//Controller Tab Profile
	@GetMapping("user/{id}")
	public ResponseEntity<List<User>> getUserById(@PathVariable("id") Long id){
		try {
			Optional<User> user = this.userRepository.findById(id);
			if(user.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(user, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
		}
	}
	
	//Controller Tab Profile
	@GetMapping("customer/{id}")
	public ResponseEntity<List<Customer>> getCustomerById(@PathVariable("id") Long id){
		try {
			Optional<Customer> custumer = this.customerRepository.findById(id);
			if(custumer.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(custumer, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<Customer>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("edit/biodata/{id}")
	public ResponseEntity<Object> editBiodata(@PathVariable("id") Long id, @RequestBody Biodata biodata){
		Optional<Biodata> biodataData = this.biodataRepository.findById(id);
		
		if(biodataData.isPresent()) {
			biodata.setId(id);
			biodata.setModifyBy("1");
			biodata.setModifyOn(Date.from(Instant.now()));
			biodata.setIsDelete(biodataData.get().getIsDelete());
			biodata.setCreatedBy(biodataData.get().getCreatedBy());
			biodata.setCreatedOn(biodataData.get().getCreatedOn());
			biodata.setImage(biodataData.get().getImage());
			biodata.setImagePath(biodataData.get().getImagePath());
			
			this.biodataRepository.save(biodata);return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	//Ubah Email
	@PutMapping("edit/user/email/{id}")
	public ResponseEntity<Object> editEmail(@PathVariable("id") Long id, @RequestBody User user){
		Optional<User> userData = this.userRepository.findById(id);
		
		if(userData.isPresent()) {
			user.setId(id);
			user.setBiodataId(userData.get().getBiodataId());
			user.setIsLocked(userData.get().getIsLocked());
			user.setIsDelete(userData.get().getIsDelete());
			user.setLoginAttempt(userData.get().getLoginAttempt());
			user.setPassword(userData.get().getPassword());
			user.setRoleId(userData.get().getRoleId());
			user.setModifyBy("1");
			user.setModifyOn(Date.from(Instant.now()));
			
			this.userRepository.save(user);return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	//Ubah DOB
	@PutMapping("edit/customer/{id}")
	public ResponseEntity<Object> editCustomer(@PathVariable("id") Long id, @RequestBody Customer customer){
		Optional<Customer> customerData = this.customerRepository.findById(id);
		
		if(customerData.isPresent()) {
			customer.setId(id);
			customer.setBiodataId(customerData.get().getBiodataId());
			Long modify = new Long("1");
			customer.setModifiedBy(modify);
			customer.setModifiedOn(Date.from(Instant.now()));
			customer.setIsDelete(customerData.get().getIsDelete());
			customer.setCreatedBy(customerData.get().getCreatedBy());
			customer.setCreatedOn(customerData.get().getCreatedOn());
			customer.setHeight(customerData.get().getHeight());
			customer.setGender(customerData.get().getGender());
			customer.setWeight(customerData.get().getWeight());
			
			this.customerRepository.save(customer);return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	//validasi Email
	@GetMapping("user/")
	public ResponseEntity<List<User>> getUserByEmail(@RequestParam("email") String email) {
		List<User> user = this.userRepository.findByEmail(email);
		return new ResponseEntity<List<User>>(user, HttpStatus.OK);
	}
	
	
	@PutMapping("edit/userlogin/{id}")
	public ResponseEntity<Object> editUserLogin(@PathVariable("id") Long id, @RequestBody User user) {
		Optional<User> userData = this.userRepository.findById(id);
		if (userData.isPresent()) {
			user.setId(userData.get().getId());
			user.setEmail(userData.get().getEmail());
			user.setPassword(userData.get().getPassword());
			user.setModifyOn(Date.from(Instant.now()));

			user.setCreatedBy(userData.get().getCreatedBy());
			user.setCreatedOn(userData.get().getCreatedOn());
			user.setBiodataId(userData.get().getBiodataId());
			user.setRoleId(userData.get().getRoleId());
			user.setDeletedBy(userData.get().getDeletedBy());
			user.setDeletedOn(userData.get().getDeletedOn());
			user.setIsDelete(userData.get().getIsDelete());
			user.setIsLocked(userData.get().getIsLocked());
			user.setLastLogin(Date.from(Instant.now()));
			user.setLoginAttempt(0);

			this.userRepository.save(user);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	//simpan reset Password
	@PostMapping("add/resetPassword")
	public ResponseEntity<Object> saveResetPassword(@RequestBody ResetPassword resetPassword){
		resetPassword.setCreatedOn(Date.from(Instant.now()));
		resetPassword.setIsDelete(false);
		
		ResetPassword resetPasswordData = this.resetPasswordRepository.save(resetPassword);
		
		if(resetPasswordData.equals(resetPassword)) {
			return new ResponseEntity<Object>("Save Data Berhasil", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	//Ubah Password
	@PutMapping("edit/user/password/{id}")
	public ResponseEntity<Object> editPassword(@PathVariable("id") Long id, @RequestBody User user){
		Optional<User> userData = this.userRepository.findById(id);
		
		if(userData.isPresent()) {
			user.setId(id);
			user.setBiodataId(userData.get().getBiodataId());
			user.setIsLocked(userData.get().getIsLocked());
			user.setIsDelete(userData.get().getIsDelete());
			user.setLoginAttempt(userData.get().getLoginAttempt());
			user.setEmail(userData.get().getEmail());
			user.setRoleId(userData.get().getRoleId());
			user.setModifyBy("1");
			user.setModifyOn(Date.from(Instant.now()));
			
			this.userRepository.save(user);return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
}

package com.miniproject.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.miniproject.demo.model.MenuRole;

public interface MenuRoleRepository extends JpaRepository<MenuRole, Long>{
	List<MenuRole> findByIsDelete(Boolean isDelete);
}

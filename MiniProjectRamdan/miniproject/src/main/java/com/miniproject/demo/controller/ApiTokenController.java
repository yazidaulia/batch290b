package com.miniproject.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.Token;
import com.miniproject.demo.repository.TokenRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiTokenController {
	@Autowired
	public TokenRepository tokenRepository;
	
	@GetMapping("token")
	public ResponseEntity<List<Token>> getTokenActive() {
		try {
			List<Token> tokenList = this.tokenRepository.findByIsExpired(false);
			return new ResponseEntity<>(tokenList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/token")
	public ResponseEntity<Object> saveToken(@RequestBody Token token){
		
		//token.setUsedFor("Register");
		token.setCreatedBy("User");
		token.setIsDelete(false);
		token.setCreatedOn(Date.from(Instant.now()));
		token.setIsExpired(false);
		
		Token tokenData = this.tokenRepository.save(token);
		
		if(tokenData.equals(token)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("tokenuser/")
	public ResponseEntity<List<Token>> getToken(@RequestParam("token") String token) {
		List<Token> tokenList = this.tokenRepository.findByToken(token);
		return new ResponseEntity<List<Token>>(tokenList, HttpStatus.OK);
	}
	
	@PutMapping("edit/token/{id}")
	public ResponseEntity<Object> editToken(@PathVariable("id") Long id,
			@RequestBody Token token){
		Optional<Token> tokenData = this.tokenRepository.findById(id);
		if(tokenData.isPresent()) {
			token.setId(id);
			token.setUsedFor(tokenData.get().getUsedFor());
			token.setModifyBy("User");
			token.setModifyOn(Date.from(Instant.now()));
			token.setCreatedBy(tokenData.get().getCreatedBy());
			token.setCreatedOn(tokenData.get().getCreatedOn());
			token.setExpiredOn(tokenData.get().getExpiredOn());
			token.setEmail(tokenData.get().getEmail());
			token.setToken(tokenData.get().getToken());
			token.setIsDelete(true);
			token.setDeletedBy("User");
			token.setDeletedOn(Date.from(Instant.now()));
			
			this.tokenRepository.save(token);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
}
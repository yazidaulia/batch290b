package com.miniproject.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.Menu;
import com.miniproject.demo.model.MenuRole;
import com.miniproject.demo.repository.MenuRepository;
import com.miniproject.demo.repository.MenuRoleRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiPageController {

	@Autowired
	MenuRoleRepository menuRoleRepository;

	@Autowired
	MenuRepository menuRepository;
	
	

	//memanggil menu role
	@GetMapping("menu")
	public ResponseEntity<List<MenuRole>> getAllMenuRole() {
		try {
			List<MenuRole> menuRole = this.menuRoleRepository.findByIsDelete(false);
			return new ResponseEntity<>(menuRole, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("MenuLain")public ResponseEntity<List<Menu>>  getMenu(){
		try {
			List<Menu> menu = this.menuRepository.findByIsDelete(false);
			return new ResponseEntity<>(menu, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	
}

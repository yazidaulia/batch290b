package com.miniproject.demo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject.demo.model.AddressModel;
import com.miniproject.demo.model.CategoryFasilitas;

public interface AddressRepository extends JpaRepository<AddressModel, Long> {

	List<AddressModel> findByIsDelete(Boolean isDelete);
	List<AddressModel> findByLabel(String label);
	
	@Query("FROM AddressModel WHERE LOWER (address) LIKE LOWER(CONCAT('%', ?1, '%')) AND isDelete=false")
	List<AddressModel> searchByAddress(String keyword);
}

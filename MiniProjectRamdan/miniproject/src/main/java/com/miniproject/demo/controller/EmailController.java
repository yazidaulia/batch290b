package com.miniproject.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.Token;
import com.miniproject.demo.service.EmailServiceForgetPassword;
import com.miniproject.demo.service.EmailServiceRegistration;

@RestController
public class EmailController {
	@Autowired private EmailServiceRegistration emailService;
	@Autowired private EmailServiceForgetPassword emailServiceForgetPass;
	 
    // Sending a simple Email
    @PostMapping("/sendMail/registration")
    public String
    sendMail(@RequestBody Token token)
    {
        String status
            = emailService.sendSimpleMail(token);
 
        return status;
    }
    
    @PostMapping("/sendMail/forgetpassword")
    public String
    sendMailResetPassword(@RequestBody Token token)
    {
        String status
            = emailServiceForgetPass.sendSimpleMail(token);
 
        return status;
    }

}
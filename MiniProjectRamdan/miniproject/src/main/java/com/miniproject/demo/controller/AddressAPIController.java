package com.miniproject.demo.controller;

import java.time.Instant;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.AddressModel;
import com.miniproject.demo.model.CategoryFasilitas;
import com.miniproject.demo.model.LocationModel;
import com.miniproject.demo.repository.AddressRepository;
import com.miniproject.demo.repository.LocationRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class AddressAPIController {

	@Autowired
	private AddressRepository addressRepository;
	@Autowired
	private LocationRepository locationRepository;

	@GetMapping("address")
	public ResponseEntity<List<AddressModel>> getAllAddress() {
		try {
			List<AddressModel> addressModel = this.addressRepository.findByIsDelete(false);
			return new ResponseEntity<>(addressModel, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("sortingByNama")
	public ResponseEntity<List<AddressModel>> getSortedName(){
		try {
			List<AddressModel> listname = addressRepository.findAll();
			List<AddressModel> listName = listname.stream().filter(name -> name.getIsDelete() == false)
					.sorted(Comparator.comparing(AddressModel::getRecipient).reversed()).collect(Collectors.toList());
			return new ResponseEntity<>(listName, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("sortingByAddress")
	public ResponseEntity<List<AddressModel>> getSortedAddress(){
		try {
			List<AddressModel> listname = addressRepository.findAll();
			List<AddressModel> listName = listname.stream().filter(address -> address.getIsDelete() == false)
					.sorted(Comparator.comparing(AddressModel::getAddress)).collect(Collectors.toList());
			return new ResponseEntity<>(listName, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("sortingByLabel")
	public ResponseEntity<List<AddressModel>> getSortedLabel(){
		try {
			List<AddressModel> listname = addressRepository.findAll();
			List<AddressModel> listName = listname.stream().filter(name -> name.getIsDelete() == false)
					.sorted(Comparator.comparing(AddressModel::getLabel).reversed()).collect(Collectors.toList());
			return new ResponseEntity<>(listName, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	
	@GetMapping("label")
	public ResponseEntity<List<AddressModel>> getAllAlamat(@RequestParam("label") String label) {
		List<AddressModel> labels = this.addressRepository.findByLabel(label);
		return new ResponseEntity<List<AddressModel>>(labels, HttpStatus.OK);
	}
	
	@GetMapping("address/search")
	public ResponseEntity<List<AddressModel>> searchAddress(@RequestParam("keyword") String keyword) {
		if (keyword.equals("")) {
			List<AddressModel> myAddress = this.addressRepository.findByIsDelete(false);
			return new ResponseEntity<List<AddressModel>>(myAddress, HttpStatus.OK);
		} else {
			List<AddressModel> myAddress = this.addressRepository.searchByAddress(keyword);
			return new ResponseEntity<List<AddressModel>>(myAddress, HttpStatus.OK);
		}
	}
	



	@PostMapping("add/address")
	public ResponseEntity<Object> saveAddress(@RequestBody AddressModel address) {
		AddressModel addressData = this.addressRepository.save(address);
		if (addressData.equals(address)) {
			address.setCreatedOn(Date.from(Instant.now()));
			this.addressRepository.save(address);
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	

	@GetMapping("address/{id}")
	public ResponseEntity<Object> getAllAddressById(@PathVariable("id") Long id) {
		try {
			Optional<AddressModel> addressModel = this.addressRepository.findById(id);
			return new ResponseEntity<>(addressModel, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	


	@GetMapping("location")
	public ResponseEntity<List<LocationModel>> getAllLocationName() {
		try {
			List<LocationModel> location = this.locationRepository.findByIsDelete(false);
			return new ResponseEntity<>(location, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("edit/address/{id}")
	public ResponseEntity<Object> editAddress(@PathVariable("id") Long id, @RequestBody AddressModel address) {
		Optional<AddressModel> addressData = this.addressRepository.findById(id);
		if (addressData.isPresent()) {
			address.setId(id);
			address.setModifiedOn(Date.from(Instant.now()));
			address.setBiodataId(addressData.get().getBiodataId());
			address.setCreatedBy(addressData.get().getCreatedBy());
			address.setCreatedOn(addressData.get().getCreatedOn());
			this.addressRepository.save(address);
			return new ResponseEntity<Object>("Edit Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("delete/address/{id}")
	public ResponseEntity<Object> deleteAddress(@PathVariable("id") Long id) {
		Optional<AddressModel> addressData = this.addressRepository.findById(id);
		if (addressData.isPresent()) {
			AddressModel address = new AddressModel();
			address.setId(id);
			address.setIsDelete(true);
			address.setDeletedBy(addressData.get().getCreatedBy());
			address.setDeletedOn(Date.from(Instant.now()));
			address.setAddress(addressData.get().getAddress());
			address.setBiodataId(addressData.get().getBiodataId());
			address.setCreatedBy(addressData.get().getCreatedBy());
			address.setCreatedOn(addressData.get().getCreatedOn());
			address.setLabel(addressData.get().getLabel());
			address.setLocationId(addressData.get().getLocationId());
			address.setModifiedBy(addressData.get().getModifiedBy());
			address.setModifiedOn(addressData.get().getModifiedOn());
			address.setPostalCode(addressData.get().getPostalCode());
			address.setRecipient(addressData.get().getRecipient());
			address.setRecipientPhoneNumber(addressData.get().getRecipientPhoneNumber());
			this.addressRepository.save(address);
			return new ResponseEntity<>("Deleted Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

}
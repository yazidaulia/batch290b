package com.miniproject.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.ResetPassword;

public interface ResetPasswordRepository extends JpaRepository<ResetPassword, Long> {

}

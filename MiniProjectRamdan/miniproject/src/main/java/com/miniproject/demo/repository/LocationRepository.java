package com.miniproject.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.LocationModel;

public interface LocationRepository extends JpaRepository<LocationModel, Long> {

	List<LocationModel> findByIsDelete(Boolean isDelete);
}

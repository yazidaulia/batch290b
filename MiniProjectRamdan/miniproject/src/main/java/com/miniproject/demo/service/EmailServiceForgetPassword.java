package com.miniproject.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.miniproject.demo.model.Token;

@Lazy(value = true)
@Configuration
@Service
public class EmailServiceForgetPassword {

	@Autowired 
	private JavaMailSender javamailsender;
	@Value("${spring.mail.username}") 
	private String sender;
 
	
    public String sendSimpleMail(Token token)
    {
 
        // Try block to check for exceptions
        try {
 
            // Creating a simple mail message
            SimpleMailMessage mailMessage
                = new SimpleMailMessage();
 
            // di gunakan buat mengirim otp
            mailMessage.setFrom(sender);
            mailMessage.setTo(token.getEmail());
            mailMessage.setText(templateSimpleMessage().getText() + "Tolong jangan sebarkan OTP ini. OTP anda adalah :"+token.getToken());
            mailMessage.setSubject("OTP");
 
            // Sending the mail
            javamailsender.send(mailMessage);
            return "Mail Sent Successfully...";
        }
 
        // Catch block to handle the exceptions
        catch (Exception e) {
        	
            return "Error while Sending Mail";
        }
    }
    @Bean
	public SimpleMailMessage templateSimpleMessage() {
		SimpleMailMessage message = new SimpleMailMessage();
		
		message.setText("Hallo!! Pelanggan yang Terhormat \n\n");
		return message;
	}

}
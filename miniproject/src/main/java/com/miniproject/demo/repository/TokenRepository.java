package com.miniproject.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.Token;

public interface TokenRepository extends JpaRepository<Token, Long>{
	List<Token> findByToken(String token);
	
	List<Token> findByIsExpired(Boolean isExpired);
}

package com.miniproject.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.CustomerRelation;

public interface CustomerRelationRepository extends JpaRepository<CustomerRelation, Long>{

}

package com.miniproject.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.BloodGroup;


public interface BloodGroupRepository extends JpaRepository<BloodGroup, Long> {

}

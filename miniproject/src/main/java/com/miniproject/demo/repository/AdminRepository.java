package com.miniproject.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.Admin;

public interface AdminRepository extends JpaRepository<Admin, Long>{

}

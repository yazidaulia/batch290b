package com.miniproject.demo.repository;


import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.Doctor_Treatment;


public interface doctor_treatmentRepository extends JpaRepository<Doctor_Treatment, BigInteger>  {
	List<Doctor_Treatment> findByIsDelete(Boolean isDelete);
}

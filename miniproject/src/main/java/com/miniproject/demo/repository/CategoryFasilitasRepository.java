package com.miniproject.demo.repository;
import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject.demo.model.CategoryFasilitas;


public interface CategoryFasilitasRepository extends JpaRepository<CategoryFasilitas, BigInteger> {
	List<CategoryFasilitas> findByIsDelete(Boolean isDelete);
	
	@Query("FROM CategoryFasilitas WHERE lower (name) LIKE lower (concat('%', ?1, '%')) AND isDelete=false")
	List<CategoryFasilitas> searchByKeyword(String keyword);

}


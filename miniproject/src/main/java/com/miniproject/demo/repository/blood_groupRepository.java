package com.miniproject.demo.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.Blood_Group;

public interface blood_groupRepository extends JpaRepository<Blood_Group, BigInteger> {
	List<Blood_Group> findByIsDelete(Boolean isDelete);
}

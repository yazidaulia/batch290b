package com.miniproject.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.Doctor;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {

}

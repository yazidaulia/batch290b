package com.miniproject.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.CustomerMember;

public interface CustomerMemberRepository extends JpaRepository<CustomerMember, Long> {
	
	List<CustomerMember> findByIsDelete(Boolean isDelete);

}

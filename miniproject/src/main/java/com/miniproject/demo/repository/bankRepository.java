package com.miniproject.demo.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.Bank;


public interface bankRepository extends JpaRepository <Bank, BigInteger> {
	List<Bank> findByIsDelete(Boolean isDelete);
}

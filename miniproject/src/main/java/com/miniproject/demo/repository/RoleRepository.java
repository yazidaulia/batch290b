package com.miniproject.demo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject.demo.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
	List<Role> findByIsDelete(Boolean isDelete);
	
	@Query("SELECT v FROM Role v WHERE LOWER(v.roleName) LIKE LOWER(CONCAT('%', ?1, '%')) "
			+ "AND isDelete=false")
	Page<Role> searchByRoleName(String roleName, Pageable pageable);
	
	@Query("FROM Role WHERE lower(roleName) LIKE lower(concat('%', ?1, '%')) AND isDelete=false")
	List<Role> searchByKeyword(String keyword);
	
	//select * from m_menu m, m_menu_role r WHERE m.parent_id = 1 and r.role_id = 2
}

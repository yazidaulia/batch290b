package com.miniproject.demo.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.Current_Doctor_Specialization;

public interface current_specializationRepository extends JpaRepository<Current_Doctor_Specialization, BigInteger>  {
	List<Current_Doctor_Specialization> findByIsDelete(Boolean isDelete);
}

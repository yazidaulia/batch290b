package com.miniproject.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.demo.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}

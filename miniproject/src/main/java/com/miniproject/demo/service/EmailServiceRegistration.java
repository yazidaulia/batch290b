package com.miniproject.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.miniproject.demo.model.Token;

@Service
public class EmailServiceRegistration {
    @Autowired private JavaMailSender javaMailSender;
    
    @Value("${spring.mail.username}") private String sender;
    
    public String sendSimpleMail(Token token)
    {
 
        // Try block to check for exceptions
        try {
 
            // Creating a simple mail message
            SimpleMailMessage mailMessage
                = new SimpleMailMessage();
 
            // Setting up necessary details
            mailMessage.setFrom(sender);
            mailMessage.setTo(token.getEmail());
            mailMessage.setText("Hi, kode OTP registrasi akunmu : "+ token.getToken());
            mailMessage.setSubject("OTP Registration Account");
 
            // Sending the mail
            javaMailSender.send(mailMessage);
            return "Mail Sent Successfully...";
        }
 
        // Catch block to handle the exceptions
        catch (Exception e) {
            return "Error while Sending Mail";
        }
    }
 
}
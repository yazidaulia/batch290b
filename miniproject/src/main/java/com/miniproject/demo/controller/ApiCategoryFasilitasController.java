package com.miniproject.demo.controller;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.CategoryFasilitas;
import com.miniproject.demo.repository.CategoryFasilitasRepository;




@RestController
@CrossOrigin
@RequestMapping("/api/")
public class ApiCategoryFasilitasController {

	@Autowired
	public CategoryFasilitasRepository categoryFasilitasRepository;
	
	@GetMapping("categoryfasilitas")
	public ResponseEntity<List<CategoryFasilitas>> getAllFasilitasCategory(){
		try {
			List<CategoryFasilitas> categoryFasilitas = this.categoryFasilitasRepository.findByIsDelete(false);
			return new ResponseEntity<>(categoryFasilitas, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping("categoryfasilitas/{id}")
	public ResponseEntity<List<CategoryFasilitas>> getCategoryFasilitasById(@PathVariable("id") BigInteger id){
		try {
			Optional<CategoryFasilitas> categoryFasilitas = this.categoryFasilitasRepository.findById(id);
			if(categoryFasilitas.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(categoryFasilitas, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<CategoryFasilitas>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/categoryfasilitas")
	public ResponseEntity<Object> saveCategoryFasilitas(@RequestBody CategoryFasilitas categoryFasilitas){
		
		BigInteger add = new BigInteger("1");
		categoryFasilitas.setCreatedBy(add);
		categoryFasilitas.setCreatedOn(Date.from(Instant.now()));
		categoryFasilitas.setIsDelete(false);
		
		CategoryFasilitas categoryFasilitasData = this.categoryFasilitasRepository.save(categoryFasilitas);
		
		if(categoryFasilitasData.equals(categoryFasilitas)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@PutMapping("edit/categoryfasilitas/{id}")
	public ResponseEntity<Object> editCategoryFasilitas(@PathVariable("id") BigInteger id, @RequestBody CategoryFasilitas categoryFasilitas){
		Optional<CategoryFasilitas> categoryFasilitasData = this.categoryFasilitasRepository.findById(id);
		
		if(categoryFasilitasData.isPresent()) {
			categoryFasilitas.setId(id);
			BigInteger modify = new BigInteger("1");
			categoryFasilitas.setModifiedBy(modify);
			categoryFasilitas.setModifiedOn(Date.from(Instant.now()));
			categoryFasilitas.setCreatedBy(categoryFasilitasData.get().getCreatedBy());
			categoryFasilitas.setCreatedOn(categoryFasilitasData.get().getCreatedOn());
			categoryFasilitas.setIsDelete(categoryFasilitasData.get().getIsDelete());
			
			this.categoryFasilitasRepository.save(categoryFasilitas);return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@PutMapping("delete/categoryfasilitas/{id}")
	public ResponseEntity<Object> deleteCategoryFasilitas(@PathVariable("id") BigInteger id){
		Optional<CategoryFasilitas> categoryFasilitasData = this.categoryFasilitasRepository.findById(id);
		
		if(categoryFasilitasData.isPresent()) {
			CategoryFasilitas categoryFasilitas = new CategoryFasilitas();
			categoryFasilitas.setId(id);
			categoryFasilitas.setIsDelete(true);
			BigInteger delete = new BigInteger("1");
			categoryFasilitas.setDeletedBy(delete);
			categoryFasilitas.setCreatedOn(Date.from(Instant.now()));
			categoryFasilitas.setCreatedBy(categoryFasilitasData.get().getCreatedBy());
			categoryFasilitas.setCreatedOn(categoryFasilitasData.get().getCreatedOn());
			categoryFasilitas.setModifiedBy(categoryFasilitasData.get().getModifiedBy());
			categoryFasilitas.setModifiedOn(categoryFasilitasData.get().getModifiedOn());
			categoryFasilitas.setName(categoryFasilitasData.get().getName());
			
			this.categoryFasilitasRepository.save(categoryFasilitas);
			return new ResponseEntity<>("Deleted Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PostMapping("categoryfasilitas/search/")
	public ResponseEntity<List<CategoryFasilitas>> getCategoryFasilitasName(@RequestParam("keyword") String keyword){
		if(keyword.equals("")) {
			List<CategoryFasilitas> categoryFasilitas = this.categoryFasilitasRepository.findByIsDelete(false);
			return new ResponseEntity<List<CategoryFasilitas>>(categoryFasilitas, HttpStatus.OK);
		} else {
			List<CategoryFasilitas> categoryFasilitas = this.categoryFasilitasRepository.searchByKeyword(keyword);
			return new ResponseEntity<List<CategoryFasilitas>>(categoryFasilitas, HttpStatus.OK);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}


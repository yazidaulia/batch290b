package com.miniproject.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/profilesharedlayout/")
public class ProfileSharedLayoutController {

	@GetMapping("index")
	public ModelAndView indexpasien() {
		ModelAndView view = new ModelAndView("/profilesharedlayout/index");
		return view;
	}
}

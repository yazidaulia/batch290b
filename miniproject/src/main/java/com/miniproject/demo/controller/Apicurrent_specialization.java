package com.miniproject.demo.controller;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.Current_Doctor_Specialization;
import com.miniproject.demo.repository.current_specializationRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class Apicurrent_specialization {
@Autowired
private current_specializationRepository current_specializationrepository;

@GetMapping("current_specialization")
public ResponseEntity<List<Current_Doctor_Specialization>> getAllJanji() {
	try {
		List<Current_Doctor_Specialization> t_current_doctor_specialization = this.current_specializationrepository.findByIsDelete(false);
		return new ResponseEntity<>(t_current_doctor_specialization, HttpStatus.OK);
	} catch (Exception e) {
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}



@GetMapping("current_specialization/{id}")
public ResponseEntity<List<Current_Doctor_Specialization>> getCurrent_Doctor_SpecializationById(@PathVariable("id") BigInteger id){
	try {
		Optional<Current_Doctor_Specialization> current_specialization = this.current_specializationrepository.findById(id);
		if(current_specialization.isPresent()) {
			ResponseEntity rest = new ResponseEntity<>(current_specialization, HttpStatus.OK);
			
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	} catch (Exception e) {
		return new ResponseEntity<List<Current_Doctor_Specialization>>(HttpStatus.NO_CONTENT);
	}
}
	

@PostMapping("add/doctor_id")
public ResponseEntity<Object> saveJanji(@RequestBody Current_Doctor_Specialization t_current_specialization) {
	
	BigInteger add = new BigInteger("1");
	
	t_current_specialization.setCreatedBy(add);
	t_current_specialization.setCreatedOn(Date.from(Instant.now()));
	t_current_specialization.setIsDelete(false);
	
	Current_Doctor_Specialization t_current_specializationData = this.current_specializationrepository.save(t_current_specialization);
	if (t_current_specializationData.equals(t_current_specialization)) {
		return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
	} else {
		return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
	}
	}
}



package com.miniproject.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.Biodata;
import com.miniproject.demo.model.CustomerMember;
import com.miniproject.demo.model.EducationLevel;
import com.miniproject.demo.repository.BiodataRepository;
import com.miniproject.demo.repository.CustomerMemberRepository;
import com.miniproject.demo.repository.CustomerRelationRepository;
import com.miniproject.demo.repository.CustomerRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiCustomerMemberController {
	
	@Autowired
	public CustomerMemberRepository customerMemberRepository;
	@Autowired
	public BiodataRepository biodataRepository;
	@Autowired
	public CustomerRepository customerRepository;
	@Autowired
	public CustomerRelationRepository customerRelationRepository;
	
	@GetMapping("customermember")
	public ResponseEntity<List<CustomerMember>> getAllCustomerMember() {
		try {
			List<CustomerMember> customerMember = this.customerMemberRepository.findByIsDelete(false);
			return new ResponseEntity<>(customerMember, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}		
	}
	
	@PostMapping("add/customermember")
	public ResponseEntity<Object> saveCustomerMember(@RequestBody CustomerMember customerMember){
		CustomerMember customerMemberData = this.customerMemberRepository.save(customerMember);
		if (customerMemberData.equals(customerMemberData)) {
			Long add = new Long("1");
			customerMember.setCreatedBy(add);
			customerMember.setCreatedOn(Date.from(Instant.now()));
			
			this.customerMemberRepository.save(customerMember);
			return new ResponseEntity<Object>("Sava Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	
}

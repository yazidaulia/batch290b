package com.miniproject.demo.controller;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.Blood_Group;
import com.miniproject.demo.repository.blood_groupRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class Apiblood_group {
	@Autowired
	public blood_groupRepository blood_grouprepository;
	
	
	@GetMapping("m_blood_group")
	public ResponseEntity<List<Blood_Group>> getAllCode() {
		try {
			List<Blood_Group> m_blood_group = this.blood_grouprepository.findByIsDelete(false);
			return new ResponseEntity<>(m_blood_group, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		

	}	
	
	@GetMapping("m_blood_group/{id}")
	public ResponseEntity<List<Blood_Group>> getBlood_GroupById(@PathVariable("id") BigInteger id) {
		try {
			Optional<Blood_Group> m_blood_group = this.blood_grouprepository.findById(id);
			if (m_blood_group.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(m_blood_group, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Blood_Group>>(HttpStatus.NO_CONTENT);
		}
	}	
	
	
	@PostMapping("add/code")
	public ResponseEntity<Object> saveCode(@RequestBody Blood_Group m_blood_group) {
		
		BigInteger add = new BigInteger("1");
		
		m_blood_group.setCreatedBy(add);
		m_blood_group.setCreatedOn(Date.from(Instant.now()));
		m_blood_group.setIsDelete(false);

		Blood_Group m_blood_groupData = this.blood_grouprepository.save(m_blood_group);

		if (m_blood_groupData.equals(m_blood_group)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);

		}
	}
	
	@PutMapping("edit/code/{id}")
	public ResponseEntity<Object> editCode(@PathVariable("id") BigInteger id,
			@RequestBody Blood_Group m_blood_group){
		
		Optional<Blood_Group> m_blood_groupData= this.blood_grouprepository.findById(id);
		
		if(m_blood_groupData.isPresent()) {
		
			m_blood_group.setId(id);
		    
		    BigInteger add = new BigInteger("1");
		    m_blood_group.setModifiedBy(add);
		    m_blood_group.setIsDelete(m_blood_groupData.get().getIsDelete());
		    m_blood_group.setModifiedOn(Date.from(Instant.now()));
		    m_blood_group.setCreatedBy(m_blood_groupData.get().getCreatedBy());
		    m_blood_group.setCreatedOn(m_blood_groupData.get().getCreatedOn());
			this.blood_grouprepository.save(m_blood_group);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		
		}else {
			return ResponseEntity.notFound().build();
			
					
		}
	}



@PutMapping("delete/code/{id}")
public ResponseEntity<Object> deleteCode(@PathVariable("id") BigInteger id,
		@RequestBody Blood_Group m_blood_group){
	
	Optional<Blood_Group> m_blood_groupData= this.blood_grouprepository.findById(id);
	
	if(m_blood_groupData.isPresent()) {
	
		m_blood_group.setId(id);
	    
	    BigInteger add = new BigInteger("1");
	    m_blood_group.setModifiedBy(add);
	    m_blood_group.setIsDelete(true);
	    m_blood_group.setDeletedBy(add);
	    m_blood_group.setDeletedOn(Date.from(Instant.now()));
	    m_blood_group.setModifiedOn(Date.from(Instant.now()));
	    m_blood_group.setCreatedBy(m_blood_groupData.get().getCreatedBy());
	    m_blood_group.setCreatedOn(m_blood_groupData.get().getCreatedOn());
		this.blood_grouprepository.save(m_blood_group);
		return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
	
	}else {
		return ResponseEntity.notFound().build();
		
				
	}
}

}






package com.miniproject.demo.controller;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.Blood_Group;
import com.miniproject.demo.model.Doctor_Treatment;
import com.miniproject.demo.repository.doctor_treatmentRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class Api_doctortreatment {
	@Autowired
	private doctor_treatmentRepository doctor_treatmentrepository;

	@GetMapping("doctor_treatment")
	public ResponseEntity<List<Doctor_Treatment>> getAllJanji() {
		try {
			List<Doctor_Treatment> doctor_treatment = this.doctor_treatmentrepository.findByIsDelete(false);
			return new ResponseEntity<>(doctor_treatment, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

@GetMapping("doctor_treatment/{id}")
public ResponseEntity<List<Doctor_Treatment>> getDoctor_TreatmentById(@PathVariable("id") BigInteger id){
	try {
		Optional<Doctor_Treatment> doctor_treatment = this.doctor_treatmentrepository.findById(id);
		if(doctor_treatment.isPresent()) {
			ResponseEntity rest = new ResponseEntity<>(doctor_treatment, HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	} catch (Exception e) {
	return new ResponseEntity<List<Doctor_Treatment>>(HttpStatus.NO_CONTENT);
	}
 }




@PostMapping("add/doctor_id")
public ResponseEntity<Object> saveJanji(@RequestBody Doctor_Treatment doctor_treatment) {
	
	BigInteger add = new BigInteger("1");
	doctor_treatment.setCreated_by(add);
	doctor_treatment.setCreatedOn(Date.from(Instant.now()));
	doctor_treatment.setIsDelete(false);
	
	Doctor_Treatment doctor_treatmentData = this.doctor_treatmentrepository.save(doctor_treatment);
	if (doctor_treatmentData.equals(doctor_treatment)) {
		return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
	} else {
		return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
	}
}
}
		
	
package com.miniproject.demo.controller;

import java.time.Instant;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.BloodGroup;
import com.miniproject.demo.repository.BloodGroupRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiBloodGroupController {
	@Autowired
	public BloodGroupRepository bloodGroupRepository;
	
	@PostMapping("add/bloodgroup")
	public ResponseEntity<Object> saveBloodGroup(@RequestBody BloodGroup bloodGroup) {
		Long add = new Long("1");
		bloodGroup.setCreatedBy(add);
		bloodGroup.setCreatedOn(Date.from(Instant.now()));
		bloodGroup.setIsDelete(false);

		BloodGroup bloodGroupData = this.bloodGroupRepository.save(bloodGroup);
		if (bloodGroupData.equals(bloodGroup)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
}

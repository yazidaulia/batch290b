package com.miniproject.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject.demo.repository.bankRepository;


@Controller
@RequestMapping("/bank/")
public class bank {
	
	@Autowired
	private bankRepository bankrepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("bank/indexapi");
		return view;
	}
	
}

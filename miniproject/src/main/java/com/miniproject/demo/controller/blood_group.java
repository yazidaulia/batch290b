package com.miniproject.demo.controller;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject.demo.model.Blood_Group;
import com.miniproject.demo.repository.blood_groupRepository;

@Controller
@RequestMapping("/blood_group/")
public class blood_group {
	
	@Autowired
	private blood_groupRepository blood_grouprepository;
	

@GetMapping("indexapi")
public ModelAndView indexapi() {
	ModelAndView view = new ModelAndView("blood_group/indexapi");
	return view;
}


}

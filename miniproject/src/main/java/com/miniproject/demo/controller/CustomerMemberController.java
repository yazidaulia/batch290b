package com.miniproject.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/pasien/")
public class CustomerMemberController {
	@GetMapping("indexpasien")
	public ModelAndView indexpasien() {
		ModelAndView view = new ModelAndView("/pasien/indexpasien");
		return view;
	}
}

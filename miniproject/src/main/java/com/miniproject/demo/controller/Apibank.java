package com.miniproject.demo.controller;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.Bank;
import com.miniproject.demo.model.Blood_Group;
import com.miniproject.demo.repository.bankRepository;
import com.miniproject.demo.repository.blood_groupRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class Apibank {
	@Autowired
	public bankRepository bankrepository;
	
	@GetMapping("m_bank")
	public ResponseEntity<List<Bank>> getAllName() {
		try {
			List<Bank> m_bank = this.bankrepository.findByIsDelete(false);
			return new ResponseEntity<>(m_bank, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

@GetMapping("m_bank/{id}")
public ResponseEntity<List<Bank>> getBlood_GroupById(@PathVariable("id") BigInteger id) {
	try {
		Optional<Bank> m_bank = this.bankrepository.findById(id);
		if (m_bank.isPresent()) {
			ResponseEntity rest = new ResponseEntity<>(m_bank, HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	} catch (Exception e) {
		return new ResponseEntity<List<Bank>>(HttpStatus.NO_CONTENT);
	}
}	


@PostMapping("add/name")
public ResponseEntity<Object> saveName(@RequestBody Bank m_bank) {
	
	BigInteger add = new BigInteger("1");
	
	m_bank.setCreatedBy(add);
	m_bank.setCreatedOn(Date.from(Instant.now()));
	m_bank.setIsDelete(false);

	Bank m_bankData = this.bankrepository.save(m_bank);

	if (m_bankData.equals(m_bank)) {
		return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
	} else {
		return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);

	}
}

@PutMapping("edit/name/{id}")
public ResponseEntity<Object> editName(@PathVariable("id") BigInteger id,
		@RequestBody Bank m_bank){
	
	Optional<Bank> m_bankData= this.bankrepository.findById(id);
	
	if(m_bankData.isPresent()) {
	
		m_bank.setId(id);
	    
	    BigInteger add = new BigInteger("1");
	    m_bank.setModifiedBy(add);
	    m_bank.setIsDelete(m_bankData.get().getIsDelete());
	    m_bank.setModifiedOn(Date.from(Instant.now()));
	    m_bank.setCreatedBy(m_bankData.get().getCreatedBy());
	    m_bank.setCreatedOn(m_bankData.get().getCreatedOn());
		this.bankrepository.save(m_bank);
		return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		
	}else {
		return ResponseEntity.notFound().build();
		
				
	}
}

@PutMapping("delete/name/{id}")
public ResponseEntity<Object> deleteName(@PathVariable("id") BigInteger id,
		@RequestBody Bank m_bank){
	
	Optional<Bank> m_bankData= this.bankrepository.findById(id);
	
	if(m_bankData.isPresent()) {
	
		m_bank.setId(id);
		
		 BigInteger add = new BigInteger("1");
		    m_bank.setModifiedBy(add);
		    m_bank.setIsDelete(true);
		    m_bank.setDeletedBy(add);
		    m_bank.setDeletedOn(Date.from(Instant.now()));
		    m_bank.setModifiedOn(Date.from(Instant.now()));
		    m_bank.setCreatedBy( m_bankData.get().getCreatedBy());
		    m_bank.setCreatedOn( m_bankData.get().getCreatedOn());
			this.bankrepository.save( m_bank);
			
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
			
	}else {
		return ResponseEntity.notFound().build();
		
				
	}
}

}
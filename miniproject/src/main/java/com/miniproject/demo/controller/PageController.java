package com.miniproject.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/page/")
public class PageController {

	@GetMapping("initialpage")
	public ModelAndView initialpage() {
		ModelAndView view = new ModelAndView("page/initialpage");
		return view;
	}
	
	@GetMapping("landingpageadmin")
	public ModelAndView landingpageadmin() {
		ModelAndView view = new ModelAndView("page/landingpageadmin");
		return view;
	}
	
	@GetMapping("landingpagedokter")
	public ModelAndView landingpagedokter() {
		ModelAndView view = new ModelAndView("page/landingpagedokter");
		return view;
	}

	@GetMapping("landingpagepasien")
	public ModelAndView landingpagepasien() {
		ModelAndView view = new ModelAndView("page/landingpagepasien");
		return view;
	}
	
	@GetMapping("landingpagefaskes")
	public ModelAndView landingpagefaskes() {
		ModelAndView view = new ModelAndView("page/landingpagefaskes");
		return view;
	}
}

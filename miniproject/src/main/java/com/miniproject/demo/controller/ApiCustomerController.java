package com.miniproject.demo.controller;

import java.time.Instant;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.Customer;
import com.miniproject.demo.repository.CustomerRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiCustomerController {
	
	@Autowired
	public CustomerRepository customerRepository;
	
	@PostMapping("add/customer")
	public ResponseEntity<Object> saveCustomer(@RequestBody Customer customer) {
		Long add = new Long("1");
		customer.setCreatedBy(add);
		customer.setCreatedOn(Date.from(Instant.now())); 
		customer.setIsDelete(false);

		Customer customerData = this.customerRepository.save(customer);
		if (customerData.equals(customer)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("add/datacustomer")
	public ResponseEntity<Object> saveDataCustomer(@RequestBody Customer customer) {
		customer.setCreatedOn(Date.from(Instant.now())); 
		customer.setIsDelete(false);

		Customer customerData = this.customerRepository.save(customer);
		if (customerData.equals(customer)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
}

package com.miniproject.demo.controller;

import java.time.Instant;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.CustomerRelation;
import com.miniproject.demo.repository.CustomerRelationRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiCustomerRelationController {

	@Autowired
	public CustomerRelationRepository customerRelationRepository;
	
	@PostMapping("add/customerrelation")
	public ResponseEntity<Object> saveCustomerReation(@RequestBody CustomerRelation customerRelation) {
		Long add = new Long("1");
		customerRelation.setCreatedBy(add);
		customerRelation.setCreatedOn(Date.from(Instant.now()));
		customerRelation.setIsDelete(false);

		CustomerRelation customerRelationData = this.customerRelationRepository.save(customerRelation);
		if (customerRelationData.equals(customerRelation)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
}

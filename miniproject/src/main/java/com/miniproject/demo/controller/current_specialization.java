package com.miniproject.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject.demo.repository.blood_groupRepository;
import com.miniproject.demo.repository.current_specializationRepository;

@Controller
@RequestMapping("/current_specialization/")

public class current_specialization {

	@Autowired
	private current_specializationRepository current_specializationrepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("current_specialization/indexapi");
		return view;
	
	}
	
}

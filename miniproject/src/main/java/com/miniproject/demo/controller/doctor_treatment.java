package com.miniproject.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject.demo.repository.doctor_treatmentRepository;

@Controller
@RequestMapping("/doctor_treatment/")

public class doctor_treatment {

	@Autowired
	private doctor_treatmentRepository doctor_treatmentrepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("doctor_treatment/indexapi");
		return view;
	
	}
	
}

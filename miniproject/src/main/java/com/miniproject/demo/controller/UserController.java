package com.miniproject.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/user/")
public class UserController {
	
	@GetMapping("login")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("user/indexapi");
		return view;
	}
	
	@GetMapping("pendaftaran")
	public ModelAndView pendaftaranapi() {
		ModelAndView view = new ModelAndView("pendaftaran/pendaftaranapi");
		return view;
	}
	
	@GetMapping("resetpassword")
	public ModelAndView forgetpassword() {
		ModelAndView view = new ModelAndView("forgetpassword/forgetpassword");
		return view;
	}
	
	@GetMapping("hakakses")
	public ModelAndView hakakses() {
		ModelAndView view = new ModelAndView("pengaturan_akses/pengaturan_akses");
		return view;
	}

}

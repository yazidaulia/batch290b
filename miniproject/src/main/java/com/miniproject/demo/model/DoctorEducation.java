package com.miniproject.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "education_doctor")
public class DoctorEducation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "doctor_id")
	private Long doctorId;
	
	@Column (name = "education_level_id")
	private Long educationLevelId;
	
	@Column (name = "institution_name")
	private String institutionName;
	
	@Column (name = "major")
	private String major;

	@Column (name = "start_year")
	private String startYear;
	
	@Column (name = "end_year")
	private String endYear;
	
	@Column (name = "is_last_education")
	private Boolean isLastEducation;
	
	@Column(name = "created_by")
	private Long createdBy;
	
	@Column(name = "created_on")
	private Date createdOn;
	
	@Column(name = "modify_by")
	private Long modifyBy;
	
	@Column(name = "modify_on")
	private Date modifyOn;
	
	@Column(name = "delete_by")
	private Long deleteBy;
	
	@Column(name = "delete_on")
	private Date deleteOn;
	
	@Column(name = "is_delete")
	private Boolean isDelete;
	
	@ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    public Biodata doctor;

    @ManyToOne
    @JoinColumn(name = "education_level_id", insertable = false, updatable = false)
    public BloodGroup educationLevel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getEducationLevelId() {
		return educationLevelId;
	}

	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getStartYear() {
		return startYear;
	}

	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	public String getEndYear() {
		return endYear;
	}

	public void setEndYear(String endYear) {
		this.endYear = endYear;
	}

	public Boolean getIsLastEducation() {
		return isLastEducation;
	}

	public void setIsLastEducation(Boolean isLastEducation) {
		this.isLastEducation = isLastEducation;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Long getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(Long modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyOn() {
		return modifyOn;
	}

	public void setModifyOn(Date modifyOn) {
		this.modifyOn = modifyOn;
	}

	public Long getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(Long deleteBy) {
		this.deleteBy = deleteBy;
	}

	public Date getDeleteOn() {
		return deleteOn;
	}

	public void setDeleteOn(Date deleteOn) {
		this.deleteOn = deleteOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Biodata getDoctor() {
		return doctor;
	}

	public void setDoctor(Biodata doctor) {
		this.doctor = doctor;
	}

	public BloodGroup getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(BloodGroup educationLevel) {
		this.educationLevel = educationLevel;
	}
	
    
}

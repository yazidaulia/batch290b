package com.miniproject.demo.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_current_specialization")
public class Current_Doctor_Specialization {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private BigInteger id;
	
	@Column(name="doctor_id")
	private BigInteger doctor_id;
	
	@Column(name="specialization_id")
	private BigInteger specialization_id;

	@Column(name="created_by")
	private BigInteger createdBy;

	@Column(name="created_on")
	private Date createdOn;

	@Column(name="modified_by")
	private BigInteger modifiedBy;

	@Column(name="modified_on")
	private Date modifiedOn;

	@Column(name="deleted_by")
	private BigInteger deletedBy;

	@Column(name="deleted_on")
	private Date deletedOn;

	@Column(name="is_delete")
	private Boolean isDelete;

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getDoctor_id() {
		return doctor_id;
	}

	public void setDoctor_id(BigInteger doctor_id) {
		this.doctor_id = doctor_id;
	}

	public BigInteger getSpecialization_id() {
		return specialization_id;
	}

	public void setSpecialization_id(BigInteger specialization_id) {
		this.specialization_id = specialization_id;
	}

	public BigInteger getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigInteger createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public BigInteger getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(BigInteger modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public BigInteger getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(BigInteger deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

}

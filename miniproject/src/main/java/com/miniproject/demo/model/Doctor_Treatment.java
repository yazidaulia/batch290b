package com.miniproject.demo.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_doctor_treatment")
public class Doctor_Treatment {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="id")
private BigInteger id;

@Column(name="doctor_id")
private BigInteger doctor_id;

@Column(name="name")
private String name;

@Column(name="created_by")
private BigInteger created_by;

@Column(name="created_on")
private Date createdOn;

@Column(name="modified_by")
private BigInteger modifiedBy;

@Column(name="modified_on")
private Date modifiedOn;

@Column(name="deleted_by")
private BigInteger deletedBy;

@Column(name="deleted_on")
private Date deletedOn;

@Column(name="is_delete")
private Boolean isDelete;

public BigInteger getId() {
	return id;
}

public void setId(BigInteger id) {
	this.id = id;
}

public BigInteger getDoctor_id() {
	return doctor_id;
}

public void setDoctor_id(BigInteger doctor_id) {
	this.doctor_id = doctor_id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public BigInteger getCreated_by() {
	return created_by;
}

public void setCreated_by(BigInteger created_by) {
	this.created_by = created_by;
}

public Date getCreatedOn() {
	return createdOn;
}

public void setCreatedOn(Date createdOn) {
	this.createdOn = createdOn;
}

public BigInteger getModifiedBy() {
	return modifiedBy;
}

public void setModifiedBy(BigInteger modifiedBy) {
	this.modifiedBy = modifiedBy;
}

public Date getModifiedOn() {
	return modifiedOn;
}

public void setModifiedOn(Date modifiedOn) {
	this.modifiedOn = modifiedOn;
}

public BigInteger getDeletedBy() {
	return deletedBy;
}

public void setDeletedBy(BigInteger deletedBy) {
	this.deletedBy = deletedBy;
}

public Date getDeletedOn() {
	return deletedOn;
}

public void setDeletedOn(Date deletedOn) {
	this.deletedOn = deletedOn;
}

public Boolean getIsDelete() {
	return isDelete;
}

public void setIsDelete(Boolean isDelete) {
	this.isDelete = isDelete;
}

}
